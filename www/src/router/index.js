import Vue from 'vue'
import Router from 'vue-router'

// const Foo = resolve => require(['../views/hello.vue'], resolve);
// import hello from '../views/hello.vue';

import main from '../views/main.vue'
import about from '../views/about.vue'
import signup from '../views/signup.vue'
import login from '../views/login.vue'
import terms from '../views/terms.vue'
import communityGuidelines from '../views/communityGuidelines.vue'
import privacy from '../views/privacy.vue'
import settings from '../views/settings.vue'
import profile from '../views/profile.vue'
import wallet from '../views/wallet.vue'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    { name: 'home', path: '/', component: main },
    { name: 'about', path: '/about', component: about },
    { name: 'signup', path: '/signup', component: signup },
    { name: 'login', path: '/login', component: login },
    { name: 'terms', path: '/terms', component: terms },
    { name: 'guidelines', path: '/guidelines', component: communityGuidelines },
    { name: 'privacy', path: '/privacy', component: privacy },
    { name: 'settings', path: '/settings', component: settings },
    { name: 'profile', path: '/profile', component: profile },
    { name: 'wallet', path: '/wallet', component: wallet }
  ]
})

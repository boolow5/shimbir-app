import q from 'q'
import axios from 'axios'
import common from './common'

var GetWallets = function () {
  var deferred = q.defer()
  axios({
    method: 'GET',
    url: common.getFullURL('/wallets'),
    headers: { 'Authorization': '' + common.getToken() || localStorage.getItem('user_token') }
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

export default {
  GetWallets
}

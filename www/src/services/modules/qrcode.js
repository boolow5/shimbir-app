import q from 'q'

var Scanner = {}

var initQRCode = function () {
  var deferred = q.defer()
  if (window.QRScanner && typeof window.QRScanner.prepare === 'function') {
    window.QRScanner.prepare(function (err, status) {
      if (err) {
        deferred.reject(err)
      } else {
        Scanner = window.QRScanner
        deferred.resolve(status)
      }
    })
  } else {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  }
  return deferred.promise
}

var scan = function () {
  var deferred = q.defer()
  if (!Scanner) {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.scan(function (err, text) {
      if (err) {
        deferred.reject(err)
      } else {
        deferred.resolve({data: text})
      }
    })
  }
  return deferred.promise
}

var cancelScan = function () {
  var deferred = q.defer()
  if (!Scanner) {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.cancelScan(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var show = function () {
  var deferred = q.defer()
  if (!Scanner) {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.show(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var hide = function () {
  var deferred = q.defer()
  if (!Scanner) {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.hide(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var enableLight = function () {
  var deferred = q.defer()
  if (!Scanner) {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.enableLight(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var disableLight = function () {
  var deferred = q.defer()
  if (!Scanner) {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.disableLight(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var useFrontCamera = function () {
  var deferred = q.defer()
  if (!Scanner) {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.useFrontCamera(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var useBackCamera = function () {
  var deferred = q.defer()
  if (!Scanner) {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.useBackCamera(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var pausePreview = function () {
  var deferred = q.defer()
  if (!Scanner) {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.pausePreview(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var resumePreview = function () {
  var deferred = q.defer()
  if (!Scanner) {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.resumePreview(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var getStatus = function () {
  var deferred = q.defer()
  if (!Scanner) {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.getStatus(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

var openSettings = function () {
  var deferred = q.defer()
  if (!Scanner) {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    if (!status.canOpenSettings) {
      deferred.reject(new Error('we do not have your permission to open settings'))
    } else {
      Scanner.openSettings()
      deferred.resolve('OK')
    }
  }
  return deferred.promise
}

var clearAll = function () {
  var deferred = q.defer()
  if (!Scanner) {
    deferred.reject(new Error('plugin QRScanner is not ready'))
  } else {
    Scanner.destroy(function (status) {
      deferred.resolve(status)
    })
  }
  return deferred.promise
}

export default {
  initQRCode,
  scan,
  cancelScan,
  show,
  hide,
  enableLight,
  disableLight,
  useFrontCamera,
  useBackCamera,
  pausePreview,
  resumePreview,
  getStatus,
  openSettings,
  clearAll
}

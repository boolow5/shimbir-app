import q from 'q'
import axios from 'axios'

const URL = 'https://photon.komoot.de/api'

var GetPlaceByQuery = function (query, options) {
  console.log('GetPlaceByQuery', {query, options})
  var deferred = q.defer()

  var url = URL + `?q=${query}`
  if (!options) {
    options = {}
  }
  if (options.limit) {
    url += `&limit=${options.limit}`
  }
  if (!options.lang) {
    url += `&lang=en`
  } else {
    url += `&lang=${options.lang}`
  }
  axios({
    method: 'GET',
    url: url
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var GetPlaceByQueryPioritized = function (query, location, options) {
  console.log('GetPlaceByQueryPioritized', {query, location, options})
  var deferred = q.defer()

  var url = URL + `?q=${query}&lat=${location.lat}&lon=${location.lon}`
  if (!options) {
    options = {}
  }
  if (options.limit) {
    url += `&limit=${options.limit}`
  }
  if (!options.lang) {
    url += `&lang=en`
  } else {
    url += `&lang=${options.lang}`
  }
  axios({
    method: 'GET',
    url: url
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

export default {
  GetPlaceByQuery,
  GetPlaceByQueryPioritized
}

import q from 'q'
import axios from 'axios'
import common from './common'

var AddTrip = function (coords1, coords2, distance, price, currency, serviceAmount) {
  var deferred = q.defer()
  if (!coords1 || !coords2) {
    deferred.reject(new Error('invalid coordinates'))
  }
  if (!price) {
    deferred.reject(new Error('invalid price'))
  }
  if (!distance) {
    deferred.reject(new Error('invalid distance'))
  }
  axios({
    method: 'POST',
    url: common.getFullURL('/trip'),
    data: {from: coords1, to: coords2, distance, price, currency, service_amount: serviceAmount},
    headers: { 'Authorization': '' + common.getToken() || localStorage.getItem('user_token') }
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var UpdateTrip = function (tripID, coords1, coords2, distance, price, currency, serviceAmount) {
  var deferred = q.defer()
  if (!coords1 || !coords2) {
    deferred.reject(new Error('invalid coordinates'))
  }
  if (!price) {
    deferred.reject(new Error('invalid price'))
  }
  if (!distance) {
    deferred.reject(new Error('invalid distance'))
  }
  if (!tripID || String(tripID).length !== 24) {
    deferred.reject(new Error('invalid trip id'))
  }
  axios({
    method: 'PUT',
    url: common.getFullURL('/trip'),
    data: {trip_id: tripID, from: coords1, to: coords2, distance, price, currency, service_amount: serviceAmount},
    headers: { 'Authorization': '' + common.getToken() || localStorage.getItem('user_token') }
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var CancelTrip = function (tripID) {
  var deferred = q.defer()
  if (!tripID) {
    deferred.reject(new Error('invalid trip id'))
  }
  axios({
    method: 'PUT',
    url: common.getFullURL('/trip/cancel'),
    data: {trip_id: tripID},
    headers: { 'Authorization': '' + common.getToken() || localStorage.getItem('user_token') }
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

export default {
  AddTrip,
  UpdateTrip,
  CancelTrip
}

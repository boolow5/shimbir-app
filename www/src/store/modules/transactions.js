const state = {
  wallets: [],
  walletMode: false
}
const getters = {
  getWallets (state) {
    return state.wallets
  },
  getIsWalletMode (state) {
    return state.walletMode
  },
  getMaxBalance (state) {
    let max = 0
    for (let i = 0; i < state.wallets.length; i++) {
      if (state.wallets[i].balance > max) {
        max = state.wallets[i].balance
      }
    }
    return max
  }
}
const mutations = {
  setWallets (state, payload) {
    if (payload && payload.constructor.name === 'Array') {
      state.wallets = payload
    }
  },
  setIsWalletMode (state, payload) {
    if (typeof payload === 'boolean') {
      state.walletMode = payload
    }
  }
}
const actions = {
  setWallets (context, payload) {
    context.commit('setWallets', payload)
  },
  setIsWalletMode (context, payload) {
    context.commit('setIsWalletMode', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}

const state = {
  messages: [],
  maxMessages: 10,
  lastMessageID: 0,
  showPopup: false,
  loading: {
    enabled: false,
    message: 'Loading...',
    extras: {},
    onCancel: function (e) {
      console.log('onCancel loading', e)
    },
    showCancel: false
  },
  staticDomain: 'http://localhost:8082/',
  version: {},
  online: false
}
const getters = {
  getVersion (state) {
    return state.version
  },
  getIsOnline (state) {
    return state.online
  },
  getStaticDir (state) {
    return function (path) {
      return state.staticDomain + path
    }
  },
  isLoading (state) {
    return state.loading
  },
  showPopup (state) {
    return state.showPopup
  },
  getLastMsgID (state) {
    return state.lastMessageID
  },
  getMessages (state) {
    return state.messages
  }
}
const mutations = {
  setVersion (state, payload) {
    if (payload && payload.hasOwnProperty('version')) {
      state.version = payload.version
    }
  },
  setOnline (state, payload) {
    if (payload && payload.hasOwnProperty('online')) {
      state.online = payload.online
    }
  },
  isLoading (state, payload) {
    if (payload && payload.hasOwnProperty('enable')) {
      state.loading.enabled = payload.enable
      if (payload.msg) {
        state.loading.message = payload.msg
        state.loading.extras = payload.extras || {}
      }
      if (typeof payload.onCancel === 'function') {
        state.loading.onCancel = payload.onCancel
      } else {
        state.loading.onCancel = null
      }
    }
  },
  showLoadingCancel (state, payload) {
    state.loading.showCancel = payload === true
  },
  setShowPopup (state, payload) {
    state.showPopup = payload
  },
  incrementMsgID (state, payload) {
    state.lastMessageID++
  },
  addMessage (state, payload) {
    state.messages.push(payload)
    if (state.messages.length > state.maxMessages) {
      state.messages.shift()
    }
  },
  removeMessage (state, payload) {
    if (!payload) {
      return
    }
    for (let i = 0; i < state.messages.length; i++) {
      if (state.messages[i] && state.messages[i].id === payload) {
        state.messages.splice(i, 1)
      }
    }
  },
  clearApp (state, payload) {
    state.lastMessageID = 0
  }
}
const actions = {
  setVersion (context, payload) {
    context.commit('setVersion', payload)
    context.commit('setOnline', payload)
  },
  setOnline (context, payload) {
    context.commit('setOnline', payload)
  },
  isLoading (context, payload) {
    console.log('isLoading', payload)
    context.commit('isLoading', payload)
    if (typeof context.getters.isLoading.onCancel === 'function') {
      setTimeout(() => {
        context.commit('showLoadingCancel', true)
      }, 5000)
    } else {
      context.commit('showLoadingCancel', false)
    }
  },
  setShowPopup (context, payload) {
    context.commit('setShowPopup', payload)
  },
  incrementMsgID (context, payload) {
    context.commit('incrementMsgID', payload)
  },
  addMessage (context, payload) {
    context.commit('addMessage', payload)
  },
  removeMessage (context, payload) {
    context.commit('removeMessage', payload)
  },
  clearAll (context, payload) {
    context.commit('clearApp', payload)
    context.commit('clearUser', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}

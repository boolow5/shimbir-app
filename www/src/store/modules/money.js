const state = {
  currency: {
    symbol: '$',
    name: 'USD'
  }
}
const getters = {
  getCurrencySymbol (state) {
    return state.currency.symbol
  }
}
const mutations = {
}
const actions = {
}

export default {
  state,
  getters,
  mutations,
  actions
}

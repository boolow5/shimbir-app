const state = {
  ownVehicles: [],
  driveVehicles: []
}
const getters = {
  getOwnVehicles (state) {
    return state.ownVehicles
  },
  getDriveVehicles (state) {
    return state.driveVehicles
  }
}
const mutations = {
  setOwnVehicles (state, payload) {
    if (typeof payload === 'object' && payload.constructor.name === 'Array') {
      state.ownVehicles = payload
    }
  },
  setDriveVehicles (state, payload) {
    if (typeof payload === 'object' && payload.constructor.name === 'Array') {
      state.driveVehicles = payload
    }
  }
}
const actions = {
  setOwnVehicles (context, payload) {
    context.commit('setOwnVehicles', payload)
  },
  setDriveVehicles (context, payload) {
    context.commit('setDriveVehicles', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}

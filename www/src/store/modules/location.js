import {EventBus} from '../../main'

const state = {
  maxRadius: 1.0, // km
  nearbyDistricts: [],
  mogadishu: {
    long: 45.34431062666954,
    lat: 2.0513121520512603,
    backed: 'qaado',
    address: {
      FormattedAddress: 'Suuqa cooska, Shibis, Mogadishu, Banaadir, Somalia',
      Street: 'Suuqa cooska',
      HouseNumber: '',
      Suburb: 'Shibis',
      Postcode: '',
      State: 'Banaadir',
      StateDistrict: '',
      County: 'Shibis',
      Country: 'Somalia',
      CountryCode: 'SO',
      City: 'Mogadishu'
    }
  },
  isMogadishu: true,
  requests: []
}
const getters = {
  tripRequests (state) {
    return state.requests
  },
  isMogadishu (state) {
    return state.isMogadishu
  },
  maxRadius (state) {
    return state.maxRadius
  },
  getNearbyDistricts (state) {
    return state.nearbyDistricts
  },
  getMogadishu (state) {
    return state.mogadishu
  }
}
const mutations = {
  replaceTripRequests (state, payload) {
    for (let i = 0; i < state.requests.length; i++) {
      if (state.requests[i].trip_id === payload.trip_id) {
        state.requests[i] = payload
        return
      }
    }
  },
  addTripRequests (state, payload) {
    if (typeof payload === 'object') {
      state.requests.push(payload)
    }
  },
  removeTripRequests (state, payload) {
    if (typeof payload === 'string') {
      for (let i = 0; i < state.requests.length; i++) {
        if (state.requests[i].trip_id === payload) {
          state.requests.slice(i, 1)
        }
      }
    }
  },
  setIsMogadishu (state, payload) {
    if (typeof payload === 'boolean') {
      state.isMogadishu = payload
    }
  },
  setMaxRadius (state, payload) {
    if (typeof payload === 'number' && payload > 0) {
      state.maxRadius = payload
    }
  },
  setNearbyDistricts (state, payload) {
    if (typeof payload === 'object' && payload.constructor.name === 'Array') {
      state.nearbyDistricts = payload
    }
  },
  clearLocationData (state) {
    state.nearbyDistricts = []
    state.requests = []
  }
}

const actions = {
  addTripRequests (context, payload) {
    if (payload && payload.hasOwnProperty('trip_id')) {
      // check if trip request is already there
      let found = {}
      for (let i = 0; i < context.getters.tripRequests.length; i++) {
        if (context.getters.tripRequests[i].trip_id === payload.trip_id) {
          // replace this and return
          context.commit('replaceTripRequests', payload)
          found = context.getters.tripRequests[i]
          console.log(`lats: ${found.start_location.lat}, ${payload.start_location.lat}\n longs: ${found.start_location.long}, ${payload.start_location.long}`)
          console.log('SSEReceived', payload)
          EventBus.$emit('SSEReceived', payload)
          return
        }
      }
      context.commit('addTripRequests', payload)
      setTimeout(() => {
        context.commit('removeTripRequests', payload)
      }, 60000)
      console.log('SSEReceived', payload)
      EventBus.$emit('SSEReceived', payload)
    }
  },
  setIsMogadishu (context, payload) {
    context.commit('setIsMogadishu', payload)
  },
  setMaxRadius (context, payload) {
    context.commit('setMaxRadius', payload)
  },
  setNearbyDistricts (context, payload) {
    context.commit('setNearbyDistricts', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}

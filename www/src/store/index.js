import Vue from 'vue'
import Vuex from 'vuex'

import app from './modules/app'
import user from './modules/user'
import money from './modules/money'
import location from './modules/location'
import transactions from './modules/transactions'
import vehicle from './modules/vehicle'

Vue.use(Vuex)

const modules = {
  app,
  user,
  money,
  location,
  transactions,
  vehicle
}

export default new Vuex.Store({
  modules,
  strict: true
})

var emailIsValid = function (email) {
  let re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
  return re.test(email)
}

var phoneIsValid = function (phone) {
  let num = cleanPhoneNumber(phone)
  let number = parseInt(num)
  let len = number.toString().length
  return len === 9 || len === 10 || len === 12
}

var cleanPhoneNumber = function (phone) {
  if (typeof phone !== 'string') {
    return ''
  }
  return phone.replace('+', '').split(' ').join('').split('-').join('')
}

export default {
  emailIsValid,
  phoneIsValid,
  cleanPhoneNumber
}
